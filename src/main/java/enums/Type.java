package enums;

public enum Type {
    TVSHOW, MOVIE;


    public static void printOptions() {
        StringBuilder options = new StringBuilder("Would you like to run TV-Show or Movie?\n");
        for (int i = 0; i < Type.values().length; i++) {
            options.append("For ").append(Type.values()[i]).append(" enter ").append(i).append("\n");
        }

        System.out.println(options.toString());
    }
}
