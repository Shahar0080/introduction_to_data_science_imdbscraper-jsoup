package enums;

public interface Genre {
    String name();

    String getPath();
}
