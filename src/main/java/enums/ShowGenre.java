package enums;

public enum ShowGenre implements Genre {
    action("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&explore=genres&genres=Action&ref_=adv_explore_rhs"),
    adventure("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Adventure&explore=genres&ref_=adv_explore_rhs"),
    scifi("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Sci-Fi&explore=genres&ref_=adv_explore_rhs"),
    fantasy("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Fantasy&explore=genres&ref_=adv_explore_rhs"),
    crime("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Crime&explore=genres&ref_=adv_explore_rhs"),
    mystery("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Mystery&explore=genres&ref_=adv_explore_rhs"),
    sport("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Sport&explore=genres&ref_=adv_explore_rhs"),
    history("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=History&explore=genres&ref_=adv_explore_rhs"),
    war("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=War&explore=genres&ref_=adv_explore_rhs"),
    documentary("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Documentary&explore=genres&ref_=adv_explore_rhs"),
    western("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Western&explore=genres&ref_=adv_explore_rhs"),
    music("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Music&explore=genres&ref_=adv_explore_rhs"),
    talkshow("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Talk-Show&explore=genres&ref_=adv_explore_rhs"),
    drama("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Drama&explore=genres&ref_=adv_explore_rhs"),
    animation("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Animation&explore=genres&ref_=adv_explore_rhs"),
    comedy("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Comedy&explore=genres&ref_=adv_explore_rhs"),
    thriller("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Thriller&explore=genres&ref_=adv_explore_rhs"),
    family("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Family&explore=genres&ref_=adv_explore_rhs"),
    romance("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Romance&explore=genres&ref_=adv_explore_rhs"),
    horror("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Horror&explore=genres&ref_=adv_explore_rhs"),
    short_("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Short&explore=genres&ref_=adv_explore_rhs"),
    realitytv("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Reality-TV&explore=genres&ref_=adv_explore_rhs"),
    gameshow("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Game-Show&explore=genres&ref_=adv_explore_rhs"),
    biography("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Biography&explore=genres&ref_=adv_explore_rhs"),
    musical("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=Musical&explore=genres&ref_=adv_explore_rhs"),
    news("https://www.imdb.com/search/title/?title_type=tv_series,tv_miniseries&genres=News&explore=genres&ref_=adv_explore_rhs");

    private final String path;

    ShowGenre(String path) {
        this.path = path;
    }

    public static void printOptions() {
        StringBuilder options = new StringBuilder("Which show genre should I scrape?\n");
        for (int i = 0; i < ShowGenre.values().length; i++) {
            options.append("For ").append(ShowGenre.values()[i]).append(" enter ").append(i).append("\n");
        }

        System.out.println(options.toString());
    }

    public String getPath() {
        return path;
    }
}
