package enums;

public enum MovieGenre implements Genre{
    action("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Action&ref_=adv_explore_rhs"),
    adventure("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Adventure&ref_=adv_explore_rhs"),
    scifi("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Sci-Fi&ref_=adv_explore_rhs"),
    fantasy("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Fantasy&ref_=adv_explore_rhs"),
    crime("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Crime&ref_=adv_explore_rhs"),
    mystery("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Mystery&ref_=adv_explore_rhs"),
    sport("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Sport&ref_=adv_explore_rhs"),
    history("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=History&ref_=adv_explore_rhs"),
    war("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=War&ref_=adv_explore_rhs"),
    documentary("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Documentary&ref_=adv_explore_rhs"),
    western("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Western&ref_=adv_explore_rhs"),
    music("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Music&ref_=adv_explore_rhs"),
    talkshow("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Talk-Show&ref_=adv_explore_rhs"),
    drama("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Drama&ref_=adv_explore_rhs"),
    animation("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Animation&ref_=adv_explore_rhs"),
    comedy("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Comedy&ref_=adv_explore_rhs"),
    thriller("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Thriller&ref_=adv_explore_rhs"),
    family("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Family&ref_=adv_explore_rhs"),
    romance("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Romance&ref_=adv_explore_rhs"),
    horror("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Horror&ref_=adv_explore_rhs"),
    short_("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Short&ref_=adv_explore_rhs"),
    realitytv("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Reality-TV&ref_=adv_explore_rhs"),
    gameshow("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Game-Show&ref_=adv_explore_rhs"),
    biography("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Biography&ref_=adv_explore_rhs"),
    musical("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Musical&ref_=adv_explore_rhs"),
    news("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=News&ref_=adv_explore_rhs"),
    filmnoir("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Film-Noir&ref_=adv_explore_rhs"),
    adult("https://www.imdb.com/search/title/?title_type=movie&explore=title_type,genres&genres=Adult&ref_=adv_explore_rhs");

    private final String path;

    MovieGenre(String path) {
        this.path = path;
    }

    public static void printOptions() {
        StringBuilder options = new StringBuilder("Which movie genre should I scrape?\n");
        for (int i = 0; i < MovieGenre.values().length; i++) {
            options.append("For ").append(MovieGenre.values()[i]).append(" enter ").append(i).append("\n");
        }

        System.out.println(options.toString());
    }

    public String getPath() {
        return path;
    }
}
