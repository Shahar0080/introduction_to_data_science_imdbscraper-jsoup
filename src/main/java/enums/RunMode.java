package enums;

public enum RunMode {
    SINGLE,SEQUENCE,RANGE;

    public static void printOptions() {
        StringBuilder options = new StringBuilder("How would you like to scrape?\n");
        for (int i = 0; i < RunMode.values().length; i++) {
            options.append("For ").append(RunMode.values()[i]).append(" enter ").append(i).append("\n");
        }

        System.out.println(options.toString());
    }
}
