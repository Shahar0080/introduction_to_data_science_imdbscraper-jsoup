package scrapers;

import enums.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import title.structure.TitleData;
import utils.Utils;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class ListScraper {


    public void beginScraping(Utils utils) throws Exception {
        Type type = utils.getTypeFromUser();
        RunMode runMode = utils.getModeFromUser();

        if (runMode == RunMode.SINGLE) {
            singleScrape(utils, type);
        } else if (runMode == RunMode.SEQUENCE) {
            sequenceScrape(utils, type);
        } else if (runMode == RunMode.RANGE) {
            System.out.println("Please enter the start: ");
            int start = (new Scanner(System.in)).nextInt();
            System.out.println("Please enter the end: ");
            int end = (new Scanner(System.in)).nextInt();
            rangeScrape(utils, type, start, end);
        }
    }

    public void singleScrape(Utils utils, Type type) throws Exception {
        Genre genre = utils.getGenreFromUser(type);

        scrape(utils, genre, type);
    }

    public void sequenceScrape(Utils utils, Type type) throws Exception {
        Genre[] genreValues = (type == Type.MOVIE) ? MovieGenre.values() : ShowGenre.values();

        for (Genre genre : genreValues) {
            scrape(utils, genre, type);
        }
    }

    public void rangeScrape(Utils utils, Type type, int start, int end) throws Exception {
        Genre[] genreValues = (type == Type.MOVIE) ? MovieGenre.values() : ShowGenre.values();

        for (int i = start; i <= end; i++) {
            scrape(utils, genreValues[i], type);
        }
    }

    public void scrape(Utils utils, Genre genre, Type type) throws Exception {
        File theDir = utils.CreateFolder();
        String fileName = theDir + "\\" + genre.name() + "_" + type.name() + ".csv";
        FileWriter csvWriter = utils.CreateCsv(fileName);

        PageScraper scraper = new PageScraper(utils);

        int totalScraped = 0;
        int totalTitles = 0;
        int counter = 0;


        String link = genre.getPath();
        Document doc = Jsoup.connect(link).get();

        utils.log(doc.title());
        do {
            Elements listerItems;

            Element listerList = doc.select("div.lister-list").first();
            listerItems = listerList.select("h3.lister-item-header");

            utils.log("Titles this page: " + listerItems.size());
            // finish whole page (normally 50 titles)
            for (Element headline : listerItems) {
                try {
                    Element h3 = headline.selectFirst("a");
                    Document movieDoc = Jsoup.connect(h3.absUrl("href")).get();
                    utils.log("Started working on title no." + totalScraped);
                    TitleData titleData = scraper.scrapeOnePage(movieDoc, type);
                    titleData.fixTitleData();
                    utils.SaveToCsv(totalScraped, csvWriter, titleData);
                    utils.log("Finished working on title no." + totalScraped);
                    totalScraped++;
                } catch (Exception e) {
                    System.out.println("ERROR working on title no." + totalScraped);
                    e.printStackTrace();
                }
                totalTitles++;
            }

            // move to next page
            utils.log("Moving to next page..");
            Document oldDoc = doc;
            doc = getNextPage(doc);
            while (counter < 3 && doc == null) {
                utils.log("Error moving to next page for the " + counter + " time. waiting 5 seconds and trying again...");
                Thread.sleep(5000);
                counter++;
                doc = getNextPage(oldDoc);
            }
        } while (doc != null && counter < 3);


        // Alert the user that the test has passed and close everything
        printSummary(utils, genre, totalScraped, totalTitles, fileName);

        utils.CloseCsv(csvWriter);
    }

    public Document getNextPage(Document curPage) {
        try {
            Document nextPage;

            Element nextButton = curPage.selectFirst("a.lister-page-next.next-page");

            nextPage = Jsoup.connect(nextButton.absUrl("href")).get();

            return nextPage;
        } catch (Exception e) {
            return null;
        }
    }

    public void printSummary(Utils utils, Genre genre, int totalScraped, int totalTitles, String fileLocation) {
        utils.log("****Finished scraping for genre: " + genre.name() + "****");
        utils.log("Titles scraped: " + totalScraped);
        utils.log("Total titles: " + totalTitles);
        utils.log("Summary: " + totalScraped + "/" + totalTitles);
        utils.log("File location:" + fileLocation);
    }
}
