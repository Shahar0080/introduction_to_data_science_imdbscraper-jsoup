package scrapers;

import enums.Type;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import title.structure.TitleData;
import title.structure.YearsRange;
import utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class PageScraper {
    private final Utils utils;

    public PageScraper(Utils utils) {
        this.utils = utils;
    }

    public TitleData scrapeOnePage(Document page, Type type) {
        TitleData titleData = new TitleData();
        Element wrapper = page.selectFirst("#wrapper");
        SetTitleWrapperData(wrapper, titleData); // Title, Years
        SetRatings(wrapper, titleData); // Ratings, RatingsAmount
        titleData.setEpisodeAmount(GetEpisodeAmount(wrapper)); // EpisodeAmount
        SetPlotSummaryData(wrapper, titleData); // Intro, Creator, StarringActress
        titleData.setSeasons(trimString(GetSeasons(wrapper))); // Seasons
        titleData.setActress(trimStringList(GetActress(wrapper))); // Actress
//        titleData.setPg(trimStringList(GetPg(wrapper))); // PG /* PGs is not saved since it requires going to a different webpage */
        titleData.setGenres(trimStringList(GetGenres(wrapper))); // Genres
        SetBottomTitleDetails(wrapper, titleData); // Countries, Languages, ReleaseDate
        SetBoxOfficeData(wrapper, titleData);

        utils.log(titleData.toString());

        return titleData;
    }
    /* ----Title, Years---- */

    private void SetTitleWrapperData(Element pageContent, TitleData titleData) {
        Element titleWrapper = pageContent.selectFirst("div.title_wrapper");
        // title
        titleData.setTitle(trimString(GetTitle(titleWrapper)));
        // years
        titleData.setYearsRange(trimYearsRange(GetYearsRange(titleWrapper)));

    }

    private String GetTitle(Element titleWrapper) {
        Element h1 = titleWrapper.selectFirst("h1");

        return h1.text();
    }

    private YearsRange GetYearsRange(Element titleWrapper) {
        try {
            Element subText = titleWrapper.selectFirst("div.subtext");
            Element range = subText.selectFirst("a[title='See more release dates']");

            String rangeAsString = range.text();

            rangeAsString = rangeAsString.substring(rangeAsString.indexOf("(") + 1);
            rangeAsString = rangeAsString.substring(0, rangeAsString.lastIndexOf(")"));

            String[] split = rangeAsString.split("–");

            return new YearsRange(split[0], split.length > 1 ? split[1] : null);
        } catch (Exception e) {
            return new YearsRange(null, null);
        }
    }

    /* ----Ratings, RatingsAmount---- */

    private void SetRatings(Element wrapper, TitleData titleData) {
        // ratings
        titleData.setRating(trimString(GetRating(wrapper)));
        // ratings amount
        titleData.setRatingsAmount(trimString(GetRatingsAmount(wrapper)));
    }

    private String GetRating(Element wrapper) {
        try {
            Element ratingsWrapper = wrapper.selectFirst("div.ratings_wrapper");
            Element ratingValue = ratingsWrapper.selectFirst("span[itemprop='ratingValue']");
            return ratingValue.text();
        } catch (Exception e) {
        }
        return null;
    }

    private String GetRatingsAmount(Element wrapper) {
        try {
            Element ratingsWrapper = wrapper.selectFirst("div.ratings_wrapper");
            Element ratingCount = wrapper.selectFirst("span[itemprop='ratingCount']");

            return ratingCount.text();
        } catch (Exception e) {
        }
        return null;
    }

    /* ----EpisodeAmount---- */

    private String GetEpisodeAmount(Element wrapper) {
        try {
            Element buttonPanel = wrapper.selectFirst("div.button_panel.navigation_panel");
            Elements bpSubHeading = buttonPanel.select("span.bp_sub_heading");

            for (Element bp : bpSubHeading) {
                if (bp.text().contains("episode"))
                    return bp.text().split(" ")[0];
            }
        } catch (Exception e) {
        }

        return null;
    }

    /* ----Intro, Creator, StarringActress---- */

    private void SetPlotSummaryData(Element wrapper, TitleData titleData) {
        Element plotSummaryWrapper = wrapper.selectFirst("div.plot_summary ");

        // intro
        titleData.setIntro(trimString(GetIntro(plotSummaryWrapper)));
        // creator
        titleData.setCreator(trimStringList(GetCreator(plotSummaryWrapper)));
        // starring actress
        titleData.setStarringActress(trimStringList(GetStarringActress(plotSummaryWrapper)));

    }

    private String GetIntro(Element plotSummaryWrapper) {
        try {
            Element summaryText = plotSummaryWrapper.selectFirst("div.summary_text");

            return summaryText.text();
        } catch (Exception e) {
        }
        return null;
    }

    private ArrayList<String> GetCreator(Element plotSummaryWrapper) {
        ArrayList<String> creators = new ArrayList<>();
        try {
            Element creditSummaryItem = plotSummaryWrapper.select("div.credit_summary_item").first();
            Elements a_s = creditSummaryItem.select("a");

            String text;
            for (Element a : a_s) {
                text = a.text();
                if (!text.equals("See full cast & crew") && !text.contains("more credit"))
                    creators.add(text);
            }
        } catch (Exception e) {
        }

        return creators;
    }

    private ArrayList<String> GetStarringActress(Element plotSummaryWrapper) {
        ArrayList<String> stars = new ArrayList<>();
        try {
            Element creditSummaryItem = plotSummaryWrapper.select("div.credit_summary_item").last();
            Elements a_s = creditSummaryItem.select("a");

            String text;
            for (Element a : a_s) {
                text = a.text();
                if (!text.equals("See full cast & crew") && !text.contains("more credit"))
                    stars.add(text);
            }
        } catch (Exception e) {
        }

        return stars;
    }

    /* ----Seasons---- */

    private String GetSeasons(Element wrapper) {
        int highest = -1;
        try {
            Element seasAndYear = wrapper.selectFirst("div.seasons-and-year-nav");
            Elements seasonARefs = seasAndYear.select("a[href*='season']");

            for (Element aRef : seasonARefs) {
                int curr = Integer.parseInt(aRef.text());
                if (curr > highest)
                    highest = curr;
            }
        } catch (Exception e) {
        }

        return String.valueOf(highest);
    }

    /* ----Actress---- */

    private ArrayList<String> GetActress(Element wrapper) {
        ArrayList<String> actress = new ArrayList<>();
        try {
            Element titleCast = wrapper.selectFirst("#titleCast");
            Elements trs = titleCast.select("tr");

            String name;
            // start at 1 to skip the title, i+=2 to skip double tags
            for (int i = 1; i < trs.size(); i += 2) {
                name = trs.get(i).text();
                try {
                    name = name.substring(0, name.indexOf(".") - 1);
                } catch (Exception e) {
                    continue;
                }
                actress.add(name);
            }
        } catch (Exception e) {
        }

        return actress;
    }

    /* ----PG---- */

    private ArrayList<String> GetPg(Element wrapper) {
        ArrayList<String> pgs = new ArrayList<>();
//        try {
//            Element titleStoryLine = wrapper.selectFirst("#titleStoryLine");
//
//            Elements txtBlocks = titleStoryLine.select("div.txt-block");
//
//            for (Element txtBlock : txtBlocks) {
//                try {
//                    Element h4 = txtBlock.selectFirst("h4");
//                    if (!h4.text().equals("Certificate:")) continue;
//
//                    String certificateText = txtBlock.text();
//
//                    String[] certificates = certificateText.split("\\|");
//
//                    String[] pgsOnPage = certificates[0].split("\\|");
//
//                    pgs.addAll(Arrays.asList(pgsOnPage));
//                } catch (Exception e) {
//                    continue;
//                }
//            }
//
//        } catch (Exception e) {
//        }
        return pgs;
    }

    /* ----Genres---- */

    private ArrayList<String> GetGenres(Element wrapper) {
        ArrayList<String> genres = new ArrayList<>();
        try {
            Element titleStoryLine = wrapper.selectFirst("#titleStoryLine");

            Elements seeMores = titleStoryLine.select("div.see-more.inline.canwrap");

            for (Element seeMore : seeMores) {
                try {
                    Element h4 = seeMore.selectFirst("h4");
                    if (!h4.text().equals("Genres:")) continue;

                    String genresText = seeMore.text();
                    String[] genresOnPage = genresText.split("\\|");

                    for (String gen : genresOnPage) {
                        if (gen.contains("Genres:"))
                            gen = gen.split(" ")[1];

                        if (IsGenre(gen))
                            genres.add(gen);
                    }
                } catch (Exception e) {
                    continue;
                }
            }

        } catch (Exception e) {
        }
        return genres;

    }

    /* ----Countries, Languages, ReleaseDate---- */

    private void SetBottomTitleDetails(Element wrapper, TitleData titleData) {
        Element titleDetails = wrapper.selectFirst("#titleDetails");

        // country
        titleData.setOriginCountries(trimStringList(GetCountries(titleDetails)));
        // language
        titleData.setLanguage(trimStringList(GetLanguage(titleDetails)));
        // release date
        titleData.setReleaseDate(trimString(GetReleaseDate(titleDetails)));
    }

    private ArrayList<String> GetCountries(Element titleDetails) {
        ArrayList<String> countries = new ArrayList<>();
        try {
            Elements textBlocks = titleDetails.select("div.txt-block");
            for (Element textBlock : textBlocks) {
                try {
                    Element h4 = textBlock.selectFirst("h4");
                    if (!h4.text().equals("Country:"))
                        continue;
                    Elements a_s = textBlock.select("a");
                    for (Element a : a_s) {
                        countries.add(a.text());
                    }
                } catch (Exception e) {
                    continue;
                }

            }
        } catch (Exception e) {
        }

        return countries;
    }

    private ArrayList<String> GetLanguage(Element titleDetails) {
        ArrayList<String> languages = new ArrayList<>();
        try {
            Elements textBlocks = titleDetails.select("div.txt-block");
            for (Element textBlock : textBlocks) {
                try {
                    Element h4 = textBlock.selectFirst("h4");

                    if (!h4.text().equals("Language:"))
                        continue;
                    Elements a_s = textBlock.select("a");
                    for (Element a : a_s) {
                        languages.add(a.text());
                    }
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
        }

        return languages;
    }

    private String GetReleaseDate(Element titleDetails) {
        String result;
        try {
            Elements textBlocks = titleDetails.select("div.txt-block");
            for (Element textBlock : textBlocks) {
                try {
                    Element h4 = textBlock.selectFirst("h4");
                    if (!h4.text().equals("Release Date:"))
                        continue;
                    result = textBlock.text();
                    result = result.replace("Release Date: ", "");
                    result = result.substring(0, result.indexOf("("));
                    return result;
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
        }

        return null;
    }

    /* ----Budget, CumulativeWorldwideGross, Runtime---- */

    private void SetBoxOfficeData(Element wrapper, TitleData titleData) {
        Element titleDetails = wrapper.selectFirst("#titleDetails");

        // Budget
        titleData.setBudget(trimString(GetBudget(titleDetails)));
        // CumulativeWorldwideGross
        titleData.setCumulativeWorldwideGross(trimString(GetCumulativeWorldwideGross(titleDetails)));
        // Runtime
        titleData.setRuntime(trimString(GetRuntime(titleDetails)));
    }

    private String GetBudget(Element titleDetails) {
        String result;
        try {
            Elements textBlocks = titleDetails.select("div.txt-block");
            for (Element textBlock : textBlocks) {
                try {
                    Element h4 = textBlock.selectFirst("h4");
                    if (!h4.text().equals("Budget:"))
                        continue;
                    result = textBlock.text();
                    result = result.replace("Budget:", "");
                    result = result.substring(0, result.indexOf("("));
                    return result;
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
        }

        return null;
    }

    private String GetCumulativeWorldwideGross(Element titleDetails) {
        String result;
        try {
            Elements textBlocks = titleDetails.select("div.txt-block");
            for (Element textBlock : textBlocks) {
                try {
                    Element h4 = textBlock.selectFirst("h4");
                    if (!h4.text().equals("Cumulative Worldwide Gross:"))
                        continue;
                    result = textBlock.text();
                    result = result.replace("Cumulative Worldwide Gross:", "");
//                    result = result.substring(0, result.indexOf("("));
                    return result;
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
        }

        return null;
    }

    private String GetRuntime(Element titleDetails) {
        String result;
        try {
            Elements textBlocks = titleDetails.select("div.txt-block");
            for (Element textBlock : textBlocks) {
                try {
                    Element h4 = textBlock.selectFirst("h4");
                    if (!h4.text().equals("Runtime:"))
                        continue;
                    result = textBlock.text();
                    result = result.replace("Runtime:", "");
                    result = result.substring(0, result.indexOf("m"));
                    return result;
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
        }

        return null;
    }
    /* ----Extra functions---- */

    private boolean IsGenre(String text) {
        if (text == null) return false;
        String[] genres = {"action", "adventure", "sci-fi", "fantasy", "crime", "mystery", "sport", "history", "war", "documentary", "western", "music", "talk-show", "drama", "animation", "comedy", "thriller", "family", "romance", "horror", "short", "reality-tv", "game-show", "biography", "musical", "news"};

        if (Arrays.stream(genres).anyMatch(text.toLowerCase()::contains))
            return true;

        return false;
    }

    private String trimString(String str) {
        try {
            return str.trim();
        } catch (Exception e) {
            return str;
        }
    }

    private ArrayList<String> trimStringList(ArrayList<String> list) {
        for (int i = 0; i < list.size(); i++) {
            String trimmed;
            try {
                trimmed = list.get(i).trim();
            } catch (Exception e) {
                trimmed = list.get(i);
            }
            list.set(i, trimmed);
        }
        return list;
    }

    private YearsRange trimYearsRange(YearsRange yearsRange) {
        try {
            yearsRange.setStart(yearsRange.getStart().trim());
        } catch (Exception e) {
            yearsRange.setStart(yearsRange.getStart());
        }

        try {
            yearsRange.setEnd(yearsRange.getEnd().trim());
        } catch (Exception e) {
            yearsRange.setEnd(yearsRange.getEnd());
        }

        return yearsRange;
    }
}
