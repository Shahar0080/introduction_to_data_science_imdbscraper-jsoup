package utils;

import enums.*;
import title.structure.TitleData;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class Utils {


    public Genre getGenreFromUser(Type type) {
        if ((type == Type.MOVIE)) {
            MovieGenre.printOptions();
        } else {
            ShowGenre.printOptions();
        }

        int a = (type == Type.MOVIE) ? getIntegerFromUserInRange(0, MovieGenre.values().length - 1) : getIntegerFromUserInRange(0, ShowGenre.values().length - 1);

        return (type == Type.MOVIE) ? MovieGenre.values()[a] : ShowGenre.values()[a];
    }

    public RunMode getModeFromUser() {
        RunMode.printOptions();

        int a = getIntegerFromUserInRange(0, RunMode.values().length - 1);

        return RunMode.values()[a];
    }

    public Type getTypeFromUser() {
        Type.printOptions();

        int a = getIntegerFromUserInRange(0, Type.values().length - 1);

        return Type.values()[a];
    }

    public int getIntegerFromUser() {
        Scanner s = new Scanner(System.in);
        do {
            if (!s.hasNextInt()) {
                log("Entered value is invalid!");
                s.next();
                continue;
            }
            return s.nextInt();
        } while (true);
    }

    public int getIntegerFromUserInRange(int start, int end) {
        do {
            int res = getIntegerFromUser();
            if ((res >= start) && (res <= end))
                return res;
            log("Entered value is invalid!");
        } while (true);
    }

    public void log(String log) {
        System.out.println(log);
    }

    public File CreateFolder() {
        File theDir = new File(".\\csv_data\\");
        if (!theDir.exists()) {
            if (!theDir.mkdirs()) {
                log("Error creating folder!! System exit!!");
                System.exit(0);
            }
        }
        return theDir;
    }

    public FileWriter CreateCsv(String fileName) {
        try {
            FileWriter csvWriter = new FileWriter(fileName);

            String[] columns = {"No.", "Title", "PG", "Genres", "StartYear", "EndYear", "Rating", "RatingsAmount", "Episodes", "Intro", "Creator", "Stars", "Seasons", "Actress", "Countries", "Languages", "ReleaseDate", "Budget", "CumulativeWorldwideGross", "Runtime"};
            for (int i = 0; i < columns.length; i++) {
                csvWriter.append(columns[i]);
                if (i + 1 < columns.length)
                    csvWriter.append(",");
            }
            csvWriter.append("\n");

            return csvWriter;
        } catch (Exception e) {
            return null;
        }
    }

    public void SaveToCsv(int no, FileWriter csvWriter, TitleData titleData) {
        try {
            csvWriter.append("\"").append(String.valueOf(no)).append("\"").append(",").append("\"").append(titleData.toCsv()).append("\"");
            csvWriter.append("\n");
        } catch (Exception e) {
            log("Exception saving to csv. Item number: " + no);
        }
    }

    public void CloseCsv(FileWriter csvWriter) {
        try {
            csvWriter.flush();
            csvWriter.close();
        } catch (Exception e) {
            log("Exception closing csv!!");
        }
    }
}
