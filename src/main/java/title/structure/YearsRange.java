package title.structure;

public class YearsRange {
    private String start;
    private String end;

    public YearsRange(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public String getStart() {
        if (start == null) return null;
        if (start.equals(" ") || start.equals(""))
            return null;
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        if (end == null) return null;
        if (end.equals(" ") || end.equals(""))
            return null;
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "[" +
                "start='" + start + '\'' +
                ", end='" + end + '\'' +
                ']';
    }
}
