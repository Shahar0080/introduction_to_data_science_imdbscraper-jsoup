package title.structure;

import java.util.ArrayList;

public class TitleData {
    private String title;
    private ArrayList<String> pg;
    private ArrayList<String> genres;
    private YearsRange yearsRange;
    private String rating;
    private String ratingsAmount;
    private String episodeAmount;
    private String intro;
    private ArrayList<String> creator;
    private ArrayList<String> starringActress;
    private String seasons;
    private ArrayList<String> actress;
    private ArrayList<String> originCountries;
    private ArrayList<String> languages;
    private String releaseDate;
    private String budget;
    private String cumulativeWorldwideGross;
    private String runtime;


    public TitleData() {
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPg(ArrayList<String> pg) {
        this.pg = pg;
    }

    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }

    public void setYearsRange(YearsRange yearsRange) {
        this.yearsRange = yearsRange;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setRatingsAmount(String ratingsAmount) {
        this.ratingsAmount = ratingsAmount;
    }

    public void setEpisodeAmount(String episodeAmount) {
        this.episodeAmount = episodeAmount;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setCreator(ArrayList<String> creator) {
        this.creator = creator;
    }

    public void setStarringActress(ArrayList<String> starringActress) {
        this.starringActress = starringActress;
    }

    public void setSeasons(String seasons) {
        this.seasons = seasons;
    }

    public void setActress(ArrayList<String> actress) {
        this.actress = actress;
    }

    public void setOriginCountries(ArrayList<String> originCountries) {
        this.originCountries = originCountries;
    }

    public void setLanguage(ArrayList<String> languages) {
        this.languages = languages;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setLanguages(ArrayList<String> languages) {
        this.languages = languages;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public void setCumulativeWorldwideGross(String cumulativeWorldwideGross) {
        this.cumulativeWorldwideGross = cumulativeWorldwideGross;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<String> getPg() {
        return pg;
    }

    public ArrayList<String> getGenres() {
        return genres;
    }

    public YearsRange getYearsRange() {
        return yearsRange;
    }

    public String getRating() {
        return rating;
    }

    public String getRatingsAmount() {
        return ratingsAmount;
    }

    public String getEpisodeAmount() {
        return episodeAmount;
    }

    public String getIntro() {
        return intro;
    }

    public ArrayList<String> getCreator() {
        return creator;
    }

    public ArrayList<String> getStarringActress() {
        return starringActress;
    }

    public String getSeasons() {
        return seasons;
    }

    public ArrayList<String> getActress() {
        return actress;
    }

    public ArrayList<String> getOriginCountries() {
        return originCountries;
    }

    public ArrayList<String> getLanguages() {
        return languages;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getBudget() {
        return budget;
    }

    public String getCumulativeWorldwideGross() {
        return cumulativeWorldwideGross;
    }

    public String getRuntime() {
        return runtime;
    }

    public void fixTitleData() {
        // if the string has " in it, it will make the csv not readable by pandas as it changes the amount of columns
        // therefore, we will replace all " with '

        setTitle(replaceForString(getTitle())); // title
        setPg(replaceForArrayList(getPg())); //pg
        setGenres(replaceForArrayList(getGenres())); // genres
        setRating(replaceForString(getRating())); // rating
        setRatingsAmount(replaceForString(getRatingsAmount())); // ratingsAmount
        setEpisodeAmount(replaceForString(getEpisodeAmount())); // episodeAmount
        setIntro(replaceForString(getIntro())); // intro
        setCreator(replaceForArrayList(getCreator())); // creator
        setStarringActress(replaceForArrayList(getStarringActress())); // starringActress
        setSeasons(replaceForString(getSeasons())); // seasons
        setActress(replaceForArrayList(getActress())); // actress
        setOriginCountries(replaceForArrayList(getOriginCountries())); // originCountries
        setLanguage(replaceForArrayList(getLanguages())); // languages
        setReleaseDate(replaceForString(getReleaseDate())); // releaseDate

    }

    public String replaceForString(String str, String oldChar, String newChar) {
        if (str == null) return null;
        String updated = str.replaceAll(oldChar, newChar);
        return updated;
    }

    public String replaceForString(String str) {
        return replaceForString(str, "\"", "'");
    }

    public ArrayList<String> replaceForArrayList(ArrayList<String> list, String oldChar, String newChar) {
        if (list == null) return null;
        for (int i = 0; i < list.size(); i++) {
            list.set(i, replaceForString(list.get(i), oldChar, newChar));
        }
        return list;
    }

    public ArrayList<String> replaceForArrayList(ArrayList<String> list) {
        return replaceForArrayList(list, "\"", "'");
    }

    public String toCsv() {
        double NaN = Double.NaN;
        return (title != null ? title.trim() : NaN) + "\"" + "," +
                "\"" + (pg != null ? pg : NaN) + "\"" + "," +
                "\"" + (genres != null ? genres : NaN) + "\"" + "," +
                "\"" + ((yearsRange.getStart() != null) ? yearsRange.getStart() : NaN) + "\"" + "," +
                "\"" + ((yearsRange.getEnd() != null) ? yearsRange.getEnd() : NaN) + "\"" + "," +
                "\"" + (rating != null ? rating : NaN) + "\"" + "," +
                "\"" + (ratingsAmount != null ? ratingsAmount : NaN) + "\"" + "," +
                "\"" + (episodeAmount != null ? episodeAmount : NaN) + "\"" + "," +
                "\"" + (intro != null ? intro : NaN) + "\"" + "," +
                "\"" + (creator != null ? creator : NaN) + "\"" + "," +
                "\"" + (starringActress != null ? starringActress : NaN) + "\"" + "," +
                "\"" + (seasons != null ? seasons : NaN) + "\"" + "," +
                "\"" + (actress != null ? actress : NaN) + "\"" + "," +
                "\"" + (originCountries != null ? originCountries : NaN) + "\"" + "," +
                "\"" + (languages != null ? languages : NaN) + "\"" + "," +
                "\"" + (releaseDate != null ? releaseDate : NaN) + "\"" + "," +
                "\"" + (budget != null ? budget : NaN) + "\"" + "," +
                "\"" + (cumulativeWorldwideGross != null ? cumulativeWorldwideGross : NaN) + "\"" + "," +
                "\"" + (runtime != null ? runtime : NaN);
    }

    @Override
    public String toString() {
        return "TitleData{" +
                "title='" + title + '\'' +
                ", pg=" + pg +
                ", genres=" + genres +
                ", yearsRange=" + yearsRange +
                ", rating='" + rating + '\'' +
                ", ratingsAmount='" + ratingsAmount + '\'' +
                ", episodeAmount='" + episodeAmount + '\'' +
                ", intro='" + intro + '\'' +
                ", creator='" + creator + '\'' +
                ", starringActress=" + starringActress +
                ", seasons='" + seasons + '\'' +
                ", actress=" + actress +
                ", originCountries=" + originCountries +
                ", languages=" + languages +
                ", releaseDate='" + releaseDate + '\'' +
                ", budget='" + budget + '\'' +
                ", cumulativeWorldwideGross='" + cumulativeWorldwideGross + '\'' +
                ", runtime='" + runtime + '\'' +
                '}';
    }
}
