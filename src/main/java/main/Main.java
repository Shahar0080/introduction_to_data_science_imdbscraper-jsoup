package main;

import scrapers.ListScraper;
import utils.Utils;

import java.util.Scanner;

/*
TODO:
    update image at pptx
 */
public class Main {
    public static void main(String[] args) {
        Utils utils = new Utils();
        try {
            new ListScraper().beginScraping(utils);
        } catch (Exception e) {
            e.printStackTrace();
        }
        utils.log("Finished running.. click any key to exit");
        new Scanner(System.in).nextLine();
    }
}
